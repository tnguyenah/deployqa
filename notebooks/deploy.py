# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.6
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# + [markdown] jp-MarkdownHeadingCollapsed=true
# ## Meimei

# +
# %env ONLY=
# %env TICKET_LIST=
# %env UNDEPLOYED=
# %env SKIP_LIST=
# %env MAX_ISSUES=

# !python .deployqa.py $meimei
# -

# ## Lee

# +
# %env ONLY=
# %env TICKET_LIST=
# %env UNDEPLOYED=
# %env SKIP_LIST=
# %env MAX_ISSUES=

# !python .deployqa.py $lee
# -

# ## Tuan

# +
# %env DRY_RUN=

# %env ONLY=
# %env TICKET_LIST=
# %env UNDEPLOYED=
# %env SKIP_LIST=
# %env MAX_ISSUES=

# !python .deployqa.py $tuan -v
# + [markdown] jp-MarkdownHeadingCollapsed=true
# ## John

# +
# %env ONLY=
# %env TICKET_LIST=
# %env UNDEPLOYED=
# %env SKIP_LIST=
# %env MAX_ISSUES=

# !python .deployqa.py $john
