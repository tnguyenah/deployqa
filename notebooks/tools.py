# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.6
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# ### Build nursing board

# !cd /inetpub/git/chartaccess/nursingboard && npm run build

# ### Restart ColdFusion

# !ansible hospitaldevelopmentWin -m win_service -a 'name="ColdFusion 2018 Application Server" state=restarted'

# ### Restart Apache

# !ansible localhost -m service -a 'name=httpd state=restarted'

# ### Get deployed ticket Win

# !ansible hospitaldevelopmentWin -m win_shell -a 'cd D:/ChartAccess_System/GIT_Clones/chartaccess; git log --oneline origin/release/rc2.0.. | findstr /c:"Merge remote-tracking branch '\''o" | %{$_.Split(" ")[4]}'

# ### Get deployed ticket Lin

# !cd /inetpub/git/chartaccess && git log  --oneline origin/release/rc2.0.. | grep "Merge remote-tracking branch 'origin" | awk '{print $5}'


