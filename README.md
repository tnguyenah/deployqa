# Hospital Dev Deployment Tool #

### What is this repository for? ###

* Hospital deployment tool in dev environment

### How do I get set up? ###

* Clone the repo (Linux and Windows)

* Login to server and run these commands
  * screen -R -D deployqa
  * cd ~/repos/deployqa
  * conda create --name deployqa python=3.9
  * conda activate deployqa
  * pip install -r requirements.txt
  * cp .env.example .env
  * ln -nsf ../deployqa.py public/.deployqa.py
  * jupytext --update-metadata '{"jupytext": null}' public/*.ipynb
  * jupytext --set-formats public//ipynb,notebooks//py notebooks/*.py --sync
  * jupyter server --generate-config
  * cp extras/jupyter_server_config.py ~/.jupyter/
  * ln -nsf ~/repos/deployqa/ansible.cfg /etc/ansible/
  * ln -nsf ~/repos/deployqa/hosts.yml /etc/ansible/
  * jupyter notebook --no-browser

### .env file
- EMAIL=your email
- API_KEY=your api key

#### Windows
- WIN_USER=your windows username
- WIN_IP=your windows ip address 
- WINP_PASSWORD=your windows password. Leave blank if ssh-key is used

#### Deployment options
- DRY_RUN=Dry-run mode. Only deploy code, no changes on ticket status or comments. Enable: 1
- NO_COMMENT=No comment on ticket. Enable: 1
- ONLY=Deploy specific QA tickets only. Space-separated string without 'AZH'. Ex: 1234 5678
- TICKET_LIST=Deploy specific QA tickets + CODE REVIEW tickets. Space-separated string without 'AZH'. Ex: 1234 5678
- SKIP_LIST=Tickets that are skipped. Space-separated string without 'AZH'. Ex: 1234 5678

#### Default configurations
- DEFAULT_GIT_DIR_WIN=D:/ChartAccess_System/GIT_Clones/chartaccess 
- DEFAULT_GIT_DIR_LIN=/inetpub/git/chartaccess
- TEMP_CHART_QA_LIN=/var/chartqa
- TEMP_CHART_QA_WIN=D:/ChartAccess_System/GIT_Clones/chartqa
- PYTHON_WINDOWS=C:/Python37/python.exe
- REPO_PATH_WINDOWS=C:/repos/deployqa
- WIN_DEPLOYMENT=Auto deploy on windows. Enable value: 1


<details>
<summary>Optional</summary>

### Ansible

  Ping lin servers
  
  ```shell
    ansible lin -m ping -v
  ```

  Ping win servers
  
  ```shell
    ansible win -m win_ping -v
  ```
  
</details>
